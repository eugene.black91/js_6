// 1. Чтобы использовать специальный символ как обычный, добавьте к нему обратную косую черту: \..
// Это и называется "экранирование символа"

// 2.Два способа оглашения функции: function expression и function declaration
// Function expression: function sayHi(){}
// Function declaration: let sayHi = function(){}

// 3.Поднятие (hoisting) — это поведение, при котором функцию или переменную можно использовать до объявления.
// Для переменной var это работает, для let/const - не работает

function createNewUser() {
  const newUser = {};
  Object.defineProperties(newUser, {
    firstName: { value: prompt("Please, enter your name"), writable: false },
    lastName: {
      value: prompt("Please, enter your last name"),
      writable: false,
    },
  });
  newUser.birthday = prompt("Enter your birthday date", "dd.mm.yy");
  const birthdayArr = newUser.birthday.split(".");
  newUser.getLogin = function () {
    return (this.firstName.charAt(0) + this.lastName).toLowerCase();
  };

  newUser.getAge = function () {
    return (
      ((new Date() -
        new Date(`${birthdayArr[2]}-${birthdayArr[1]}-${birthdayArr[0]}`)) /
        (24 * 3600 * 362.25 * 1000)) |
      0
    );
  };

  newUser.getPassword = function () {
    return (
      this.firstName.charAt(0).toUpperCase() +
      this.lastName.toLowerCase() +
      `${birthdayArr[2]}`
    );
  };

  return newUser;
}

const user = createNewUser();
console.log(user.getAge());
console.log(user.getPassword());
